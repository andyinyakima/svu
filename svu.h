#ifndef SVU_H
#define SVU_H

#include <QApplication>
#include <QMainWindow>
#include <QDesktopWidget>
#include <QProcess>
#include <QDir>
#include <QFileDialog>
#include <QMessageBox>
#include <QTextStream>
#include <QFont>
#include <QtNetwork/QNetworkInterface>
#include <QThread>

namespace Ui {
class svu;
}

class svu : public QMainWindow
{
    Q_OBJECT

public:
    explicit svu(QWidget *parent = 0);
    ~svu();


private slots:

    bool inputStreams();

    void VLMconfigCall();

    void createStreams();

    void ffStreams();

    void editVLMconfig();

    void showNmapData();

    void helpBrowser();

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();


    void on_outPort_spinBox_valueChanged(int anum);

private:
    Ui::svu *ui;
    QProcess *qnmap;
    QProcess *port_nmap;
    int scrnhigh;
    int scrnwide;
    int wide = 240;
    int high = 120;
    int posx = 20;
    int posy = 20;

};

#endif // SVU_H
