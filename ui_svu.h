/********************************************************************************
** Form generated from reading UI file 'svu.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SVU_H
#define UI_SVU_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_svu
{
public:
    QWidget *centralWidget;
    QGroupBox *groupBox;
    QRadioButton *createStreamsRadioButton;
    QRadioButton *editRadioButton;
    QPushButton *pushButton;
    QLabel *label_6;
    QSpinBox *svwide_spinBox;
    QSpinBox *svhigh_spinBox;
    QLabel *label_7;
    QLabel *label_8;
    QSpinBox *xpos_spinBox;
    QLabel *label_9;
    QLabel *label_10;
    QSpinBox *ypos_spinBox;
    QLabel *label_11;
    QPushButton *pushButton_2;
    QTextEdit *textEdit;
    QGroupBox *input_groupBox;
    QLineEdit *portlineEdit;
    QLineEdit *urllineEdit;
    QLabel *label_2;
    QLabel *label;
    QSpinBox *outPort_spinBox;
    QLabel *label_5;
    QLineEdit *stream_out_ID_lineEdit;
    QLabel *label_4;
    QLineEdit *programslineEdit;
    QLabel *label_3;
    QLineEdit *names_lineEdit;
    QLabel *label_12;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *svu)
    {
        if (svu->objectName().isEmpty())
            svu->setObjectName(QStringLiteral("svu"));
        svu->resize(454, 438);
        centralWidget = new QWidget(svu);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(0, 0, 211, 171));
        createStreamsRadioButton = new QRadioButton(groupBox);
        createStreamsRadioButton->setObjectName(QStringLiteral("createStreamsRadioButton"));
        createStreamsRadioButton->setGeometry(QRect(0, 40, 161, 22));
        createStreamsRadioButton->setChecked(false);
        editRadioButton = new QRadioButton(groupBox);
        editRadioButton->setObjectName(QStringLiteral("editRadioButton"));
        editRadioButton->setGeometry(QRect(0, 20, 151, 22));
        editRadioButton->setChecked(true);
        pushButton = new QPushButton(groupBox);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(160, 10, 51, 31));
        QPalette palette;
        QBrush brush(QColor(112, 174, 121, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Highlight, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Highlight, brush);
        QBrush brush1(QColor(174, 174, 174, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Disabled, QPalette::Highlight, brush1);
        pushButton->setPalette(palette);
        QFont font;
        font.setPointSize(20);
        pushButton->setFont(font);
        pushButton->setAutoFillBackground(false);
        label_6 = new QLabel(groupBox);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(0, 100, 41, 17));
        svwide_spinBox = new QSpinBox(groupBox);
        svwide_spinBox->setObjectName(QStringLiteral("svwide_spinBox"));
        svwide_spinBox->setGeometry(QRect(0, 70, 61, 27));
        svwide_spinBox->setMinimum(40);
        svwide_spinBox->setMaximum(640);
        svwide_spinBox->setSingleStep(10);
        svwide_spinBox->setValue(160);
        svhigh_spinBox = new QSpinBox(groupBox);
        svhigh_spinBox->setObjectName(QStringLiteral("svhigh_spinBox"));
        svhigh_spinBox->setGeometry(QRect(90, 70, 61, 27));
        svhigh_spinBox->setMinimum(30);
        svhigh_spinBox->setMaximum(480);
        svhigh_spinBox->setSingleStep(10);
        svhigh_spinBox->setValue(120);
        label_7 = new QLabel(groupBox);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(90, 100, 41, 17));
        label_8 = new QLabel(groupBox);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(70, 80, 16, 17));
        xpos_spinBox = new QSpinBox(groupBox);
        xpos_spinBox->setObjectName(QStringLiteral("xpos_spinBox"));
        xpos_spinBox->setGeometry(QRect(0, 120, 61, 27));
        xpos_spinBox->setMinimum(20);
        xpos_spinBox->setMaximum(3840);
        xpos_spinBox->setSingleStep(1);
        label_9 = new QLabel(groupBox);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setGeometry(QRect(10, 150, 31, 17));
        label_10 = new QLabel(groupBox);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setGeometry(QRect(70, 130, 16, 16));
        ypos_spinBox = new QSpinBox(groupBox);
        ypos_spinBox->setObjectName(QStringLiteral("ypos_spinBox"));
        ypos_spinBox->setGeometry(QRect(90, 120, 61, 24));
        ypos_spinBox->setMaximum(2040);
        ypos_spinBox->setValue(20);
        label_11 = new QLabel(groupBox);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setGeometry(QRect(100, 150, 41, 16));
        pushButton_2 = new QPushButton(groupBox);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(170, 140, 41, 27));
        textEdit = new QTextEdit(centralWidget);
        textEdit->setObjectName(QStringLiteral("textEdit"));
        textEdit->setGeometry(QRect(10, 180, 441, 221));
        input_groupBox = new QGroupBox(centralWidget);
        input_groupBox->setObjectName(QStringLiteral("input_groupBox"));
        input_groupBox->setGeometry(QRect(230, 0, 211, 181));
        portlineEdit = new QLineEdit(input_groupBox);
        portlineEdit->setObjectName(QStringLiteral("portlineEdit"));
        portlineEdit->setGeometry(QRect(0, 50, 41, 27));
        urllineEdit = new QLineEdit(input_groupBox);
        urllineEdit->setObjectName(QStringLiteral("urllineEdit"));
        urllineEdit->setGeometry(QRect(0, 20, 131, 27));
#ifndef QT_NO_TOOLTIP
        urllineEdit->setToolTip(QStringLiteral("<html><head/><body><p>This is the IP address of the stream; make sure it is the same as the host stream coming in.</p></body></html>"));
#endif // QT_NO_TOOLTIP
        urllineEdit->setToolTipDuration(-1);
        label_2 = new QLabel(input_groupBox);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(40, 60, 31, 17));
        label = new QLabel(input_groupBox);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(140, 30, 31, 17));
        outPort_spinBox = new QSpinBox(input_groupBox);
        outPort_spinBox->setObjectName(QStringLiteral("outPort_spinBox"));
        outPort_spinBox->setGeometry(QRect(70, 50, 44, 27));
        outPort_spinBox->setMinimum(19);
        outPort_spinBox->setMaximum(99);
        outPort_spinBox->setValue(19);
        label_5 = new QLabel(input_groupBox);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(120, 60, 91, 17));
        stream_out_ID_lineEdit = new QLineEdit(input_groupBox);
        stream_out_ID_lineEdit->setObjectName(QStringLiteral("stream_out_ID_lineEdit"));
        stream_out_ID_lineEdit->setGeometry(QRect(0, 80, 131, 27));
        label_4 = new QLabel(input_groupBox);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(140, 90, 61, 17));
        programslineEdit = new QLineEdit(input_groupBox);
        programslineEdit->setObjectName(QStringLiteral("programslineEdit"));
        programslineEdit->setGeometry(QRect(0, 110, 131, 27));
        label_3 = new QLabel(input_groupBox);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(140, 120, 61, 20));
        names_lineEdit = new QLineEdit(input_groupBox);
        names_lineEdit->setObjectName(QStringLiteral("names_lineEdit"));
        names_lineEdit->setGeometry(QRect(0, 140, 131, 31));
        label_12 = new QLabel(input_groupBox);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setGeometry(QRect(140, 154, 71, 20));
        svu->setCentralWidget(centralWidget);
        mainToolBar = new QToolBar(svu);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        svu->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(svu);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        svu->setStatusBar(statusBar);
        QWidget::setTabOrder(urllineEdit, portlineEdit);
        QWidget::setTabOrder(portlineEdit, outPort_spinBox);
        QWidget::setTabOrder(outPort_spinBox, stream_out_ID_lineEdit);
        QWidget::setTabOrder(stream_out_ID_lineEdit, programslineEdit);
        QWidget::setTabOrder(programslineEdit, names_lineEdit);
        QWidget::setTabOrder(names_lineEdit, svwide_spinBox);
        QWidget::setTabOrder(svwide_spinBox, svhigh_spinBox);
        QWidget::setTabOrder(svhigh_spinBox, xpos_spinBox);
        QWidget::setTabOrder(xpos_spinBox, ypos_spinBox);
        QWidget::setTabOrder(ypos_spinBox, pushButton_2);
        QWidget::setTabOrder(pushButton_2, textEdit);
        QWidget::setTabOrder(textEdit, editRadioButton);
        QWidget::setTabOrder(editRadioButton, createStreamsRadioButton);
        QWidget::setTabOrder(createStreamsRadioButton, pushButton);

        retranslateUi(svu);

        QMetaObject::connectSlotsByName(svu);
    } // setupUi

    void retranslateUi(QMainWindow *svu)
    {
        svu->setWindowTitle(QApplication::translate("svu", " Stream Views UDP", 0));
        groupBox->setTitle(QApplication::translate("svu", "Function", 0));
#ifndef QT_NO_TOOLTIP
        createStreamsRadioButton->setToolTip(QApplication::translate("svu", "<html><head/><body><p>IF 'Show Individual Views' is clicked and you press GO then you will create individual UDP addresses for each Program stream and then create view windows for those addresses.</p></body></html>", 0));
#endif // QT_NO_TOOLTIP
        createStreamsRadioButton->setText(QApplication::translate("svu", "Show Individual Views", 0));
#ifndef QT_NO_TOOLTIP
        editRadioButton->setToolTip(QApplication::translate("svu", "<html><head/><body><p>Press GO when 'Check Incoming Stream' is clicked will check if URL Host is UP and if the stream PORT is open. If PORT is closed you need to fix it.</p></body></html>", 0));
#endif // QT_NO_TOOLTIP
        editRadioButton->setText(QApplication::translate("svu", "Ck Incoming Stream", 0));
#ifndef QT_NO_TOOLTIP
        pushButton->setToolTip(QApplication::translate("svu", "<html><head/><body><p>The GO button is used twice , First to Check incoming stream..Second when Show Individual Streams is clicked. </p></body></html>", 0));
#endif // QT_NO_TOOLTIP
        pushButton->setText(QApplication::translate("svu", "Go", 0));
        label_6->setText(QApplication::translate("svu", "Width", 0));
        label_7->setText(QApplication::translate("svu", "Height", 0));
        label_8->setText(QApplication::translate("svu", "x", 0));
#ifndef QT_NO_TOOLTIP
        xpos_spinBox->setToolTip(QApplication::translate("svu", "<html><head/><body><p>If you create a second instance of 'svu' then you may want to change this value so that the views start lower on the screen.  </p></body></html>", 0));
#endif // QT_NO_TOOLTIP
        label_9->setText(QApplication::translate("svu", "Xpos", 0));
        label_10->setText(QApplication::translate("svu", "x", 0));
        label_11->setText(QApplication::translate("svu", "Ypos", 0));
        pushButton_2->setText(QApplication::translate("svu", "List", 0));
        textEdit->setHtml(QApplication::translate("svu", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:'Noto Sans';\"><br /></p></body></html>", 0));
        input_groupBox->setTitle(QApplication::translate("svu", "Inputs", 0));
#ifndef QT_NO_TOOLTIP
        portlineEdit->setToolTip(QApplication::translate("svu", "<html><head/><body><p>This must be the port number of the host stream coming in.</p></body></html>", 0));
#endif // QT_NO_TOOLTIP
        portlineEdit->setText(QApplication::translate("svu", "1230", 0));
        urllineEdit->setInputMask(QString());
        urllineEdit->setText(QApplication::translate("svu", "192.168.1.5", 0));
        label_2->setText(QApplication::translate("svu", "Port", 0));
        label->setText(QApplication::translate("svu", "URL", 0));
#ifndef QT_NO_TOOLTIP
        outPort_spinBox->setToolTip(QApplication::translate("svu", "<html><head/><body><p>This is the start of the rtp port 30xx; the xx part is added automatically. If you call up another instance of &quot;sv&quot;  increment this value to reflect the number instances of &quot;sv&quot;.</p></body></html>", 0));
#endif // QT_NO_TOOLTIP
        label_5->setText(QApplication::translate("svu", "Out Port Start", 0));
#ifndef QT_NO_TOOLTIP
        stream_out_ID_lineEdit->setToolTip(QApplication::translate("svu", "<html><head/><body><p>This has to be one word only of alphanumeric input. NO SPACES or Special Characters. </p></body></html>", 0));
#endif // QT_NO_TOOLTIP
        stream_out_ID_lineEdit->setText(QApplication::translate("svu", "svu", 0));
        label_4->setText(QApplication::translate("svu", "Stream ID", 0));
#ifndef QT_NO_TOOLTIP
        programslineEdit->setToolTip(QApplication::translate("svu", "<html><head/><body><p>You must know the Program numbers of the services you want to stream; a comma is the only delimiter that is understood. The numbers need to correlate to host stream program numbers.</p></body></html>", 0));
#endif // QT_NO_TOOLTIP
        programslineEdit->setText(QApplication::translate("svu", "3", 0));
        programslineEdit->setPlaceholderText(QString());
        label_3->setText(QApplication::translate("svu", "Programs", 0));
        names_lineEdit->setText(QApplication::translate("svu", "name-1", 0));
        label_12->setText(QApplication::translate("svu", "Names", 0));
    } // retranslateUi

};

namespace Ui {
    class svu: public Ui_svu {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SVU_H
