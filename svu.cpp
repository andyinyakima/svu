/*
 *	svu (stream views), will input a http: stream using a url and port then disseminate as single and scable viewable
 *  or audible streams on a single local viewable screen
 *
 *  Copyright (C) 2016  Andy Laberge (andylaberge@linux.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	If you did not receive a copy (most probable) of the GNU General Public License
 *	along with this program; go see <http://www.gnu.org/licenses/>.
 */

#include "svu.h"
#include "ui_svu.h"
#include <QtNetwork/QNetworkInterface>





svu::svu(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::svu)
{

    ui->setupUi(this);
    helpBrowser();


    QNetworkInterface *inter = new QNetworkInterface();
    QList<QHostAddress> list;
    list=inter->allAddresses();
    //QString str;
    ui->urllineEdit->clear();
    ui->urllineEdit->insert( list.at(2).toString());


//    for (int i=0;i<4;i++)
//    ui->textEdit->insertPlainText(list.at(i).toString()+"\n");

}

QObject *parent;

QProcess *cmdlnVLM = new QProcess(parent);



svu::~svu()
{
    cmdlnVLM->kill();
    delete ui;
}


bool svu::inputStreams()
{

    QString prog = "nmap";
    QStringList argu;

    ui->textEdit->clear();
    qnmap = new QProcess(this);

    connect(qnmap,SIGNAL(readyReadStandardOutput()),this,SLOT(showNmapData()));

    argu<<"-p"<<ui->portlineEdit->text()<<ui->urllineEdit->text();

    qnmap->start(prog,argu);
    qnmap->waitForStarted();



}

void svu::showNmapData()
{

    QString nmapRead = qnmap->readAllStandardOutput();

    ui->textEdit->append(nmapRead);
     qnmap->waitForFinished(5); // comment out will
    ui->textEdit->insertPlainText("Make sure your HOST is UP and PORT is OPEN!\n");

}



void svu::VLMconfigCall()
{
    // this part will call the built xxxx_vlm.config using vlc

    QString prog ="cvlc"; // using cvlc eliminates interface
    QStringList argu ;
    QString vlmconf ="--vlm-conf";

    QString vlmconf_path = QDir::homePath();

    vlmconf_path.append("/");
    vlmconf_path.append(ui->stream_out_ID_lineEdit->text());
    vlmconf_path.append("_vlm.conf");

    argu<<vlmconf<<vlmconf_path;


    cmdlnVLM->start(prog,argu);
}

void svu::createStreams()
{
    //this is where multiple instances of vlc are called to
    // view individual program instances on local network
    // with different ports

    int i;
    int cnt;
    QDesktopWidget desktop;   // get desktop screen info ex: 1366x768

    int desktopHeight=desktop.geometry().height(); // height = 768
    int desktopWidth=desktop.geometry().width();  // width =1366


    QString vt;  //video title
    QString prog; // mpv or ffplay
    QString rtplocal; // local net 127.0.0.1
    QString tstring; // used for parsing data
    QString nstring; // used for parsing names
    QString geo;  // geometry for individual views
    QString width; // width of view
    QString height; // height of view
    QString xpos; // view position x start
    QString ypos; // view position y start
    QString audsync;
    QString forceview = "--force-window"; //this is needed to give a window to audio
    QString no_osc = "--no-osc"; // this takes out the onscreen control
    QString font_sz= "--osd-font-size=55";
    QString osd_bar = "--osd-bar"; // this allows an osd
    QString messag = "--osd-msg1=VBR ${=video-bitrate} ABR ${=audio-bitrate}"; //bitrates in cycles
 //   QString messag = "--osd-msg1=ABR ${=audio-bitrate}";
    QString vparam = "--osd-msg1=${=video-params}"; // video parameters
    QStringList argu; // argument list for QProcess
    QStringList list; // for parsing
    QStringList prognames;
    QString tnum;
    QString tport; // port number for UDP address
    QString rtpPort; //UDP address

    wide = ui->svwide_spinBox->value();
    high = ui->svhigh_spinBox->value();
    posy = ui->ypos_spinBox->value();
    posx = ui->xpos_spinBox->value();


    // develop loop counter by countin commas
    tstring=ui->programslineEdit->text(); // list of entered program numbers
    nstring=ui->names_lineEdit->text(); // list of entered names
    cnt=tstring.count(QRegExp(","));  //
    // add one for missing comma, add another cause loop starts at 1
    cnt+=2;
    // while here make an array
    list=tstring.split(",");
    prognames=nstring.split(",");

    width=QString::number(ui->svwide_spinBox->value()); // convert interger to string for arguments
    height=QString::number(ui->svhigh_spinBox->value());

    //build views
    for(i=1;i<cnt;i++)
    {
        geo="--geometry=";  // mpv geometry call

        xpos=QString::number(posx);
        ypos=QString::number(posy);
    //    ui->xpos_spinBox->setValue(posx);
    //    ui->ypos_spinBox->setValue(posy);


        geo.append(width+"x"+height);
        geo.append("+");
        geo.append(xpos);
        geo.append("+");
        geo.append(ypos);

            rtplocal="udp://@127.0.0.1:";

        argu.clear();
        rtpPort.clear();
        vt="--title=";
        tnum=QString::number(i);
        tport=ui->portlineEdit->text();
        rtpPort= QString::number(ui->outPort_spinBox->value());

        if(i<10)
        rtpPort.append("0");
        rtpPort.append(tnum);
        rtplocal.append(rtpPort);
    //    vt.append(ui->stream_out_ID_lineEdit->text());
    //    vt.append(" Prg. ");
        vt.append(prognames.at(i-1));

// see TODO
        /*
          if(ui->mpvosc_checkBox->isChecked()==true)
          {
              argu<<rtplocal<<vt<<geo<<forceview<<osd_bar<<messag;
          }
          else if(ui->mpvosc_checkBox->isChecked()==false)
          {
               argu<<rtplocal<<geo<<forceview<<no_osc<<font_sz<<osd_bar<<messag;

           }
          */
                 argu<<rtplocal<<vt<<geo<<forceview<<osd_bar<<messag;
              //  argu<<rtplocal<<vt<<geo<<forceview<<no_osc<<font_sz<<osd_bar<<messag;
               prog="mpv";
               QProcess *vlcrtp = new QProcess(this);
                 vlcrtp->start(prog,argu);



           posx=posx+wide;
           if(posx>desktopWidth-(posx/4))
            {
               posx=20;
               posy=posy+(high+20+10);
           }

           ui->xpos_spinBox->setValue(posx);
           ui->ypos_spinBox->setValue(posy);
    }

        }

void svu::ffStreams()
{
    //this is where multiple instances of vlc are called to
    // view individual program instances on local network
    // with different ports

    int i;
    int cnt;
    QDesktopWidget desktop;

    int desktopHeight=desktop.geometry().height();
    int desktopWidth=desktop.geometry().width();


  //  QString audio = "--no-volume-save";

    QString vt;
    QString prog;
    QString rtplocal;
    QString tstring;
    QString geo;
    QString x = "-x";
    QString width;
    QString y = "-y";
    QString height;
    QString guix = "-gui_x";
    QString xpos;
    QString guiy = "-gui_y";
    QString ypos;
    QString forceview = "--force-window";

    QStringList argu;
    QStringList list;
    QString tnum;
    QString tport;
    QString rtpPort;

    wide = ui->svwide_spinBox->value();
    high = ui->svhigh_spinBox->value();


    // develop loop counter by countin commas
    tstring=ui->programslineEdit->text();
    cnt=tstring.count(QRegExp(","));
    // add one for missing comma, add another cause loop starts at 1
    cnt+=2;
    // while here make an array
    list=tstring.split(",");

    width=QString::number(ui->svwide_spinBox->value());
    height=QString::number(ui->svhigh_spinBox->value());

    for(i=1;i<cnt;i++)
    {
        geo="--geometry=";

        xpos=QString::number(posx);
        ypos=QString::number(posy);


        geo.append(width+"x"+height);
        geo.append("+");
        geo.append(xpos);
        geo.append("+");
        geo.append(ypos);

            rtplocal="udp://@127.0.0.1:";

        argu.clear();
        rtpPort.clear();
        vt="--video-title=";
        tnum=QString::number(i);
        tport=ui->portlineEdit->text();
        rtpPort= QString::number(ui->outPort_spinBox->value());

        if(i<10)
        rtpPort.append("0");
        rtpPort.append(tnum);
        rtplocal.append(rtpPort);
        vt.append(ui->stream_out_ID_lineEdit->text());
        vt.append(" Prg. ");
        vt.append(list.at(i-1));


          argu<<rtplocal<<x<<width<<y<<height;


          prog="ffplay";
          QProcess *vlcrtp = new QProcess(this);
          vlcrtp->start(prog,argu);

           posx=posx+wide;
           if(posx>desktopWidth-(posx/4))
            {
               posx=20;
               posy=posy+(high+20+10);
           }

    }

 }



void svu::editVLMconfig()
{
// this part will take the input information and create a VLM
// config



    QString tstring;
    QString tport;
    QString tnum;
 //   QString rtpSetup="del all\n\n";
    QString rtpPort;
    QString rtpRepeat = "dst=rtp{mux=ts,ttl=12,dst=127.0.0.1,port=";
    QString rtpProg = "},select= program=";
    QString rtpProgA;
    QString rtpSetup;
    QStringList list;
    QString homepath = QDir::homePath();

    //homepath.append("/svu2");

    int cnt;
    int i;


    // let's clear edit screen
    ui->textEdit->clear();
    // develop loop counter by countin commas
    tstring=ui->programslineEdit->text();
    cnt=tstring.count(QRegExp(","));
    // add one for missing comma, add another cause loop starts at 1
    cnt+=2;
    // while here make an array
    list=tstring.split(",");


   // ui->textEdit->insertPlainText(rtpPort+"\n\n");


    // enter "del all" may remove this later
    ui->textEdit->insertPlainText(rtpSetup);
    rtpSetup.clear();

    //write first line of VLM configuration "new xxxx broadcast enabled"
    rtpSetup="new "+ui->stream_out_ID_lineEdit->text()+" broadcast enabled\n";
    ui->textEdit->insertPlainText(rtpSetup);
    rtpSetup.clear();

    //next line is input
    rtpSetup="setup "+ui->stream_out_ID_lineEdit->text()+" input http://";
    rtpSetup.append(ui->urllineEdit->text());
    rtpSetup.append(":"+ui->portlineEdit->text()+"\n");
    ui->textEdit->insertPlainText(rtpSetup);
    rtpSetup.clear();

    //now the programs option
    rtpSetup="setup "+ui->stream_out_ID_lineEdit->text()+" option programs="+ui->programslineEdit->text()+"\n\n";
    ui->textEdit->insertPlainText(rtpSetup);
    rtpSetup.clear();

    //now the output .. output for some reason or another needs single or double quotes to enclose it
    rtpSetup="setup "+ui->stream_out_ID_lineEdit->text()+" output '#duplicate{";
    // now we append multiple times

        rtpRepeat="dst=udp{mux=ts,ttl=12,dst=127.0.0.1:";

    for(i=1;i<cnt;i++)
    {
        rtpSetup.append(rtpRepeat);
        tnum=QString::number(i);
      //  rtpPort.clear();
        tport=ui->portlineEdit->text();
        rtpPort= QString::number(ui->outPort_spinBox->value());
        if(i<10)
        rtpPort.append("0");
        rtpPort.append(tnum);
        rtpSetup.append(rtpPort);
        rtpProgA=rtpProg;
        rtpProgA.append(list.at(i-1));
        rtpSetup.append(rtpProgA+",");

    }

    rtpSetup.append("}'\n\n");
    ui->textEdit->insertPlainText(rtpSetup);
    rtpSetup.clear();

    // after "new" and "setup" comes "control"

    rtpSetup="control "+ui->stream_out_ID_lineEdit->text()+" play \n";
    ui->textEdit->insertPlainText(rtpSetup);
    rtpSetup.clear();

    homepath.append("/");
    homepath.append(ui->stream_out_ID_lineEdit->text());
    homepath.append("_vlm.conf");


    QFile file(homepath);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
             return ;
    QTextStream out(&file);

    out<<ui->textEdit->toPlainText();

    statusBar()->showMessage(tr("File - %1 - Saved")
            .arg(homepath));

    return;


}

void svu::helpBrowser()
{
    ui->textEdit->clear();

    QString mksure = "Step1: Please make sure your host http:// stream is running. This is easy to forget!! Try pinging for an echo if unsure.\n\n";
    ui->textEdit->insertPlainText(mksure);

    mksure = "Step2: URL box contains host address.. just the address no Port. \n\n";
    ui->textEdit->insertPlainText(mksure);

    mksure = "Step3: Port box is where host port must be correctly entered.\n\n";
    ui->textEdit->insertPlainText(mksure);

    mksure = "Step4: Out Port Start must be unique to each different svu instance you have running at the same time. "
             "If this is the first of instance of svu then 30 is ok. If this is the second instance of svu use 31, third instance use 32 and so on.\n\n";
    ui->textEdit->insertPlainText(mksure);

    mksure = "Step5: Stream ID has to be unique to each svu instance running and 1 word only of alphanumerics with no spaces or special characters.\n\n";
    ui->textEdit->insertPlainText(mksure);

    mksure = "Step6: Programs box is where program numbers must be entered with only comma delimiters last one has no comma. Also they must correlate to your host stream.\nEx-> 13,4,57\nEx-> 10\n\n";
    ui->textEdit->insertPlainText(mksure);

    mksure = "Step7: Make sure Create VLM conf button is active; then press GO. A VLM configure file will be created and posted.\n\n";
    ui->textEdit->insertPlainText(mksure);

    mksure = "Step8: After seeing your VLM conf file created, click the Create Streams button and press GO again!\n\n";
    ui->textEdit->insertPlainText(mksure);

    mksure = "PLEASE scroll to Top and start with Step1:\n";
    ui->textEdit->insertPlainText(mksure);
}

void svu::on_pushButton_clicked()
{
    // check radio buttons and do
    if(ui->createStreamsRadioButton->isChecked()==true)
    {
        editVLMconfig();
        VLMconfigCall();
/*
        if(ui->ffplay_checkBox->isChecked()==true)
        {
            ffStreams();
        }
        else
        {
             createStreams();
        }
*/
        createStreams();

    }
    else if(ui->editRadioButton->isChecked()==true)
      {
           inputStreams();

      }

 }

void svu::on_pushButton_2_clicked()
{
    QString  line;
   // helpBrowser();
    ui->textEdit->clear();

        QString homepath = QDir::homePath();
        homepath.append("/svu/svulist.txt");
        QFile filein(homepath);
        filein.open(QIODevice::ReadOnly | QIODevice::Text);

        QTextStream streamin(&filein);

    //    ui->textBrowser1->insertPlainText("\n ATSC==============================\n\n");

        while(!filein.atEnd())
        {
            line=filein.readLine(0);
            ui->textEdit->insertPlainText(line);
        }

    return;
}


void svu::on_outPort_spinBox_valueChanged(int arg1)
{
    QString lineID = "SVU ID:";
    QString linePM = "SVU PM:";
    QString lineVT = "SVU VT:";
    QString num;
    QString line;

    int foundID;
    int foundPM;
    int foundVT;
    int len;


    num=QString::number(arg1);
    lineID.prepend(num);
    linePM.prepend(num);
    lineVT.prepend(num);

    ui->stream_out_ID_lineEdit->clear();
    ui->programslineEdit->clear();
    ui->names_lineEdit->clear();


    QString homepath = QDir::homePath();
    homepath.append("/svu/svulist.txt");
    QFile filein(homepath);
    filein.open(QIODevice::ReadOnly | QIODevice::Text);

    QTextStream streamin(&filein);




    while(!filein.atEnd())
    {
        line=filein.readLine(0);
      //    ui->textEdit->insertPlainText(line);
        foundID=line.indexOf(lineID);
        if(foundID!=-1)
        {
            line.remove(0,9);
            line=line.simplified();
            line.replace(" ","");
            ui->stream_out_ID_lineEdit->clear();
            ui->stream_out_ID_lineEdit->insert(line);

        }

        foundPM=line.indexOf(linePM);
        if(foundPM!=-1)
        {
            line.remove(0,9);
            line = line.simplified();
            line.replace(" ","");
            ui->programslineEdit->clear();
            ui->programslineEdit->insert(line);
        }

        foundVT=line.indexOf(lineVT);
        if(foundVT!=-1)
        {
            //line.remove("\n");
            line.remove(0,9);
            len=line.length();
            line.truncate(len);
            lineVT=line;
            ui->names_lineEdit->clear();
            ui->names_lineEdit->insert(line);
        }



    }






   // ui->stream_out_ID_lineEdit->insert(lineID);
   // ui->programslineEdit->insert(linePM);
   // ui->names_lineEdit->insert(lineVT);



}
